//
//  MovieCell.swift
//  ProwernessTest
//
//  Created by Kamlesh on 26/06/18.
//  Copyright © 2018 VenuIQ. All rights reserved.
//

import UIKit

class MovieCell: UITableViewCell {

    @IBOutlet weak var movieImage:AyncImageView!
    @IBOutlet weak var movieTitleLabel:UILabel!
    @IBOutlet weak var movieReleaseLabel:UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureMovieCell(_ data:Movie) {
        
        movieTitleLabel.text = data.movieName
        movieReleaseLabel.text = data.time
        if let movieImageUrl = data.smallUrl{
            movieImage.loadAsyncFrom(url:movieImageUrl, placeholder: #imageLiteral(resourceName: "image_placeholder"))
        }
    }
    
}
