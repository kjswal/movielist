//
//  MovieDetailVC.swift
//  ProwernessTest
//
//  Created by Kamlesh on 26/06/18.
//  Copyright © 2018 VenuIQ. All rights reserved.
//

import UIKit

class MovieDetailVC: UIViewController {

    @IBOutlet var movieImage:AyncImageView!
    @IBOutlet var movieNameLabel:UILabel!
    
    var movie :Movie?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.additionalViewSetup()
    }

    func additionalViewSetup() {
        if let movie = movie{
            if let largeurl = movie.largeUrl{
                movieImage.loadAsyncFrom(url:largeurl, placeholder:nil)
            }
            movieNameLabel.text = movie.movieName
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
