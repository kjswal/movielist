//
//  MovieListVC.swift
//  ProwernessTest
//
//  Created by Kamlesh on 26/06/18.
//  Copyright © 2018 VenuIQ. All rights reserved.
//

import UIKit



class MovieListVC: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var table:UITableView!
    var moviesArray = [Movie]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.additionalViewSetUp()
        self.showMovieList()
    }

    // MARK: -- Additional View Setup --
    func additionalViewSetUp() {
        
        self.table.register(UINib(nibName: "MovieCell", bundle: nil), forCellReuseIdentifier: "MovieCell")
        table.estimatedRowHeight = 86.0
        table.rowHeight = UITableViewAutomaticDimension
        self.table.tableFooterView = UIView(frame: CGRect.zero)
        
        table.delegate = self
        table.dataSource = self
    }


    // MARK: -- Table Delegates --
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return moviesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let optionCell = tableView .dequeueReusableCell(withIdentifier: "MovieCell", for: indexPath) as! MovieCell
        let moview = moviesArray[indexPath.row]
        optionCell.configureMovieCell(moview)
        return optionCell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            // handle delete (by removing the data from your array and updating the tableview)
            MovieBL.sharedInstance.deleteMovie(movie: moviesArray[indexPath.row])
            table.beginUpdates()
            moviesArray.remove(at:indexPath.row)
            table.deleteRows(at:[indexPath], with: .none)
            table.endUpdates()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let vc:MovieDetailVC = MovieDetailVC(nibName: "MovieDetailVC", bundle: nil)
        vc.movie = moviesArray[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    // show movie list from db if not cached then call service
    func showMovieList() {
        moviesArray = MovieBL.sharedInstance.getAllMoviesList()
        if moviesArray.count == 0{
            self.callMovieListApi()
        }else{
            table.reloadData()
        }

    }
    
    
    // call api to get movie list
    func callMovieListApi() {
        WebServiceHandler.sharedInstance.getMovieList(successBlock: { (response) in
            self.moviesArray = response
            DispatchQueue.main.async {
                self.table.reloadData()
            }
        }, failureBlock: { (error) in
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
