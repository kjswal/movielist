//
//  WebServiceHandler.swift
//  ProwernessTest
//
//  Created by Kamlesh on 26/06/18.
//  Copyright © 2018 VenuIQ. All rights reserved.
//

import UIKit

class WebServiceHandler: NSObject {

    static let sharedInstance:WebServiceHandler = {
        
        let instance = WebServiceHandler()
        return instance
    }()

    
    func getMovieList(successBlock:@escaping (_ result:[Movie])->Void,failureBlock:@escaping (_ error:Error)-> Void) {
        
        let urlString = "https://api.androidhive.info/json/glide.json"
        guard let url = URL(string: urlString) else { return }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            if error == nil{
                guard let data = data else { return }
                //Implement JSON decoding and parsing
                do {
                    //Decode retrived data with JSONDecoder and assing type of Article object
                    // let articlesData = try JSONDecoder().decode([Article].self, from: data)
                    let json = try JSONSerialization.jsonObject(with: data, options:.mutableContainers)
                    
                    MovieBL.sharedInstance.saveMovieListData(json as! [AnyObject])
                    
                    let movies = MovieBL.sharedInstance.getAllMoviesList()
                    successBlock(movies)
                } catch let jsonError {
                    failureBlock(jsonError)
                    print(jsonError)
                }
            }else{
                failureBlock(error!)
            }
            }.resume()
    }
}
