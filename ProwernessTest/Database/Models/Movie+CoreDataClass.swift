//
//  Movie+CoreDataClass.swift
//  ProwernessTest
//
//  Created by Kamlesh on 26/06/18.
//  Copyright © 2018 VenuIQ. All rights reserved.
//
//

import Foundation
import CoreData

public class Movie: NSManagedObject {

    func initWithDict(movieDict:[String:Any])
    {
        if let movieTitle = movieDict["name"] as? String{
            self.movieName = movieTitle
        }

        if let movieUrls = movieDict["url"] as? [String:Any]{
            
            if let smallUrl = movieUrls["small"] as? String{
                self.smallUrl = smallUrl
            }
            
            if let mediumUrl = movieUrls["medium"] as? String{
                self.mediumUrl = mediumUrl
            }

            if let largeUrl = movieUrls["large"] as? String{
                self.largeUrl = largeUrl
            }

        }

        if let movieTime = movieDict["timestamp"] as? String{
            self.time = movieTime
        }

    }
    
}
