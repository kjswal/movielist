//
//  Movie+CoreDataProperties.swift
//  ProwernessTest
//
//  Created by Kamlesh on 26/06/18.
//  Copyright © 2018 VenuIQ. All rights reserved.
//
//

import Foundation
import CoreData


extension Movie {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Movie> {
        return NSFetchRequest<Movie>(entityName: "Movie")
    }

    @NSManaged public var movieName: String?
    @NSManaged public var time: String?
    @NSManaged public var smallUrl: String?
    @NSManaged public var mediumUrl: String?
    @NSManaged public var largeUrl: String?

}
