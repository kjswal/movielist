//
//  MovieCell.swift
//  ProwernessTest
//
//  Created by Kamlesh on 26/06/18.
//  Copyright © 2018 VenuIQ. All rights reserved.
//


import UIKit
import CoreData

class MovieBL: NSObject {
    
    static let sharedInstance:MovieBL = {
        
        let instance = MovieBL()
        return instance
    }()
    
    
    
    func saveMovieListData(_ data:[AnyObject])
    {
        if(data.count > 0)
        {
            for dict in data
            {
                saveMovieListInDB(movieDict:dict as! [String : AnyObject])
            }
        }
    }
    
    func saveMovieListInDB(movieDict:[String:AnyObject])
    {
        let managedContext = CoreDataModel.sharedInstance.managedObjectContext
        var movie : Movie!
        
        let entity = NSEntityDescription.entity(forEntityName: "Movie", in: managedContext)
        movie = NSManagedObject.init(entity: entity!, insertInto: managedContext) as! Movie
        movie.initWithDict(movieDict: movieDict)
        do {
            try managedContext.save()
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
    }
    
    // fetch all movie list from db
    func getAllMoviesList()->[Movie]{
        let managedContext = CoreDataModel.sharedInstance.managedObjectContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Movie")
        
        var moviessArray = [Movie]()
        do
        {
            let list = try managedContext.fetch(fetchRequest) as? [NSManagedObject]
            moviessArray = list as! [Movie]
        } catch let error as NSError {
            print("Fetch failed: \(error.localizedDescription)")
        }
        return moviessArray
    }
    
    // delete movie from db
    func deleteMovie(movie:Movie)
    {
        let managedContext = CoreDataModel.sharedInstance.managedObjectContext
        managedContext.delete(movie)
        do {
            try managedContext.save()
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
        
    }
    
}
